/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coa;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

/**
 *
 * @author Gnimnek
 */
public class Musica 
{
    public void musica()
    {
        //para que sirva este codigo hay que AGREGAR las librerias --> basicplayer3.0 y jl1.0
        FileInputStream file;
        Player player;
        try {
            //pide la ruta de la canción
            file = new FileInputStream("src/Package/Musica/Super Nuko World.mp3");
            BufferedInputStream buf = new BufferedInputStream(file);
            Player repro;
            //crea el reproductor
            repro = new Player(buf);
            //hace sonar la musica
            repro.play();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(COA.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JavaLayerException ex) {
            Logger.getLogger(Musica.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
