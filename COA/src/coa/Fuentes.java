/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coa;

import java.util.Objects;

/**
 *
 * @author Gnimnek
 */
public class Fuentes 
{
    String nombre;

    public Fuentes(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.nombre);
        return hash;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if (obj == null)
            return false;
        else if (obj == this)
            return true;
        else if (obj instanceof Fuentes) 
        {
            Fuentes fuente = (Fuentes) obj;
            if (this.getNombre().equals(fuente.getNombre()))
                return true;
        }
        return false;
    }
    
    
}
