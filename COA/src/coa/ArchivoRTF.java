package coa;
import com.google.common.io.Files;
import static java.awt.Color.*;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.zip.*;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import org.docx4j.dml.*;
import org.docx4j.dml.wordprocessingDrawing.Inline;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.*;
import org.docx4j.wml.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.imageio.ImageIO;
import org.docx4j.model.structure.SectionWrapper;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.relationships.Relationship;
import org.docx4j.wml.P.Hyperlink;
import org.docx4j.wml.R.LastRenderedPageBreak;
/**
 *
 * @author Owner
 */
public class ArchivoRTF 
{
    private String cabezaC= "{\\rtf1";
    private String cuerpo= "";
    private String cabezaF= "";
    private String defini="";

    public ArchivoRTF() 
    {
        cabezaC+="{\\colortbl;\\red0\\green0\\blue255;\\red0\\green255\\blue255;\\red0\\green255\\blue0;";
        cabezaC+="\\red255\\green0\\blue255;\\red255\\green0\\blue0;\\red255\\green255\\blue0;";
        cabezaC+="\\red0\\green0\\blue102;\\red0\\green102\\blue102;\\red0\\green102\\blue0;";
        cabezaC+="\\red102\\green0\\blue102;\\red102\\green0\\blue0;\\red102\\green102\\blue102;";
        cabezaC+="\\red64\\green64\\blue64;\\red192\\green192\\blue192;";
        cabezaF+="{\\fonttbl{\\f0 \\fmodern \\fcharset0 Arial }";
    }
    
    public File CrearRTF(String nomb, String dir) throws IOException
    {
        File nueRTF= new File(dir+nomb+".rtf");
        BufferedWriter bw;
        if(nueRTF.createNewFile())
        {
            bw = new BufferedWriter(new FileWriter(nueRTF));
            bw.write(cabezaC);
            bw.write(cabezaF);
            bw.write(defini);
            bw.write(cuerpo);
            bw.write("}");
            bw.close();
        }
        return nueRTF;
        
    }
    public void setCuerpo(String insert) {
        this.cuerpo += insert;
    }
    
    public String ConvertirdorRTF (WordprocessingMLPackage doc, String docZip) throws Docx4JException
    {
        List parf=new ArrayList();
        R run;
        ArrayList colores=new ArrayList(), fuentes=new ArrayList(), margenes=new ArrayList();
        String TxtDoc="", TxtCel="", Nombre="", Imagen="", ParProp="", id="";
        Text btexto;
        Colores color;
        Fuentes font;
        int bd=0, bdf=0, i=0,j=0,k=0, l=0,imgCont=0, band=0;
        RPr propiedades=null;
        Br salto = null;
        Hyperlink link = null;
        ArrayList<String> resal= new ArrayList();
        JAXBElement temp2=null, tempT=null, tempC=null, templink1=null, templink2=null;
        MainDocumentPart Main = doc.getMainDocumentPart();
        fuentes.add((Fuentes)new Fuentes("Arial"));
        parf=Main.getContent();
        llenarResal(resal, colores);
        margenes=margins(doc);
        defini+="\n \\margt"+margenes.get(0)+" \\margl"+margenes.get(1)+" \\margr"+margenes.get(2)+" \\margb"+margenes.get(3)+" \n";
        //lee todo el documento
        for(j=0; j<parf.size(); j++)
        {
            //coge cada elemento del documento
            if(parf.get(j) instanceof JAXBElement)
            {
               tempT=(JAXBElement)parf.get(j);   
            }
            //Tablas
            if(tempT!=null && tempT.getValue() instanceof Tbl)
            {
                Tbl tabla= (Tbl)tempT.getValue();
                //cada vez que ya cogio una nueva tabla pone el objeto en blanco
                tempT=null;
                String align="";
                if(tabla.getTblPr()!=null)
                {
                    //ancho=tabla.getTblPr().getTblW().getW().floatValue();
                    if(tabla.getTblPr().getJc()!=null)
                    align=DetAlign(tabla);
                }
                TxtDoc+= "\n\\par\n\\tword";
                //crea una nueva tabla
                TxtDoc+="\\trgaph170";
                TxtDoc+="\\trq"+align+"\n";
                for(int a=0; a< tabla.getContent().size() ;a++)
                {
                              
                    if(tabla.getContent().get(a) instanceof Tr)
                    {
                        
                        //propiedades filas rtf
                        TxtDoc+="\\trbrdrt\\brdrs\\brdrw10\n\\trbrdrl\\brdrs\\brdrw10\n\\trbrdrb\\brdrs\\brdrw10\n\\trbrdrr\\brdrs\\brdrw10\n\n";
                        Tr fila=(Tr)tabla.getContent().get(a);
                        //indica que se va a escribir una celda
                        TxtCel+="\\intbl";
                        for(int b=0; b< fila.getContent().size(); b++ )
                        {
                            //Setea las propiedades de las celdas en rtf
                            TxtDoc+="\\clbrdrt\\brdrw15\\brdrs\n"+"\\clbrdrl\\brdrw15\\brdrs\n"+"\\clbrdrb\\brdrw15\\brdrs\n" +"\\clbrdrr\\brdrw15\\brdrs\n";
                            if(fila.getContent().get(b) instanceof JAXBElement)
                            {
                               tempC=(JAXBElement)fila.getContent().get(b);   
                            }
                            //en caso de existir una celda
                            if(tempC!=null && tempC.getValue() instanceof Tc)
                            {
                                Tc celda=(Tc)tempC.getValue();
                                int anchoC=(int)celda.getTcPr().getTcW().getW().floatValue();
                                //pone el ancho de la celda
                                TxtDoc+="\\cellx"+((anchoC+1)*(b+1))+"\n\n";
                                //genera el texto en la celda
                                TxtCel+=AccesarParrafos(celda.getContent(), resal ,colores, fuentes, this, doc );
                            }
                            //final de celda
                            TxtCel+="\\cell";
                        }
                        //señala el final de fila
                        TxtCel+="\\row\n";
                    }
                    //señala el final de las propiedades
                    TxtCel+="\\pard\n\n";
                    
                }
                TxtDoc+=TxtCel;
            }
            //revisa cada parrafo
            if(parf.get(j) instanceof P)
            {
                P temp1= (P)parf.get(j);
                //Enter
                if (temp1.getRsidRDefault() != null)
                {
                    TxtDoc+= "\\pard\\par ";
                }
                TxtDoc+="\n";                                          
                //obtiene el contenido del parrafo
                for(k=0; k<temp1.getContent().size(); k++)
                {
                    band = 0;
                    ParProp="";
                    if(temp1.getPPr() != null)
                    {
                        //Alineación
                        if( temp1.getPPr().getJc()!= null)
                        {
                            switch(temp1.getPPr().getJc().getVal())
                            {
                                case BOTH:
                                {
                                    ParProp+=" \\qj ";
                                    break;
                                }
                                case LEFT:
                                {
                                    ParProp+=" \\ql ";
                                    break;
                                }
                                case CENTER:
                                {
                                    ParProp+=" \\qc ";
                                    break;
                                }
                                case RIGHT:
                                {
                                    ParProp+=" \\qr ";
                                    break;
                                }
                            }
                        }
                        else
                        {
                            ParProp+=" \\ql ";
                        }
                        //Estilos Kenimg C.
                        if ( temp1.getPPr().getPStyle() != null)
                        {
                            band = 1;
                            if (temp1.getPPr().getPStyle().getVal() != null)
                            {
                                Nombre = temp1.getPPr().getPStyle().getVal();
                                TxtDoc+=estilosD(doc, Nombre, fuentes, colores);
                            }
                        }
                        //Espaciado
                        if(temp1.getPPr().getSpacing()!= null)
                        {
                            if( temp1.getPPr().getSpacing().getLine()!= null)
                            {
                                ParProp+=" \\sl"+temp1.getPPr().getSpacing().getLine().intValue()+" ";
                            }
                            else
                            {
                                ParProp+=" \\sl0 ";
                            }
                            if( temp1.getPPr().getSpacing().getAfter()!= null)
                            {
                                ParProp+=" \\sa"+temp1.getPPr().getSpacing().getAfter().intValue()+" ";
                            }
                            else
                            {
                                ParProp+=" \\sa0 ";
                            }
                            if( temp1.getPPr().getSpacing().getBefore()!= null)
                            {
                                ParProp+=" \\sb"+temp1.getPPr().getSpacing().getBefore().intValue()+" ";
                            }
                            else
                            {
                                ParProp+="\\sb0 ";   
                            }    
                        }
                        else
                        {
                            ParProp+=" \\sl0 \\sa0 \\sb0";
                        }
                        if(temp1.getPPr().getShd() != null && temp1.getPPr().getShd().getFill() != null)
                        {
                            color=CrearColor(temp1.getPPr().getShd().getFill());//esta funcion esta en la parte de abajo
                            int c=0;
                            for(c=0; c<colores.size(); c++)
                            {
                                if(color.equals(colores.get(c)))
                                {
                                    bd=1;
                                    break;
                                }
                            }
                            // en caso de que el color del run no este en la lista de
                            //colores conocidos, se lo añade a la lista y a la cabecera
                            if(bd==0)   
                            {
                                colores.add((Colores)color);

                                cabezaC+="\\red"+color.getRed()+"\\green"+color.getGreen()+ "\\blue" +color.getBlue()+";";
                            }
                            ParProp+="\\cbpat"+c+" ";
                            bd=0;
                        }
                        else
                        {
                            ParProp+="\\cbpat";
                        }
                        if(temp1.getPPr().getInd() != null)
                        {
                            if(temp1.getPPr().getInd().getFirstLine()!= null)
                            {
                                ParProp+=" \\fi"+temp1.getPPr().getInd().getFirstLine().intValue()+" ";
                            }
                            else
                            {
                                ParProp+=" \\fi0 ";
                            }
                            if(temp1.getPPr().getInd().getLeft()!= null)
                            {
                                ParProp+=" \\li"+temp1.getPPr().getInd().getLeft().intValue()+" ";
                            }
                            else
                            {
                                ParProp+=" \\li0 ";
                            }
                            if(temp1.getPPr().getInd().getRight()!= null)
                            {
                                ParProp+=" \\ri"+temp1.getPPr().getInd().getRight().intValue()+" ";
                            }
                            else
                            {
                                ParProp+=" \\ri0 ";
                            }
                        }
                        else
                        {
                            ParProp+="\\li0 \\fi0 \\ri0 ";
                        }
                        
                    }
                    else
                    {
                        ParProp+="\\ql \\sl0 \\sa0 \\sb0 \\li0 \\fi0 \\ri0 \\cbpat ";
                    }
                    //Hypervinculos
                    if (temp1.getContent().get(k) instanceof JAXBElement)
                    {
                        templink1 = (JAXBElement) temp1.getContent().get(k);
                        if (templink1.getValue() instanceof Hyperlink)
                        {
                            link = (Hyperlink) templink1.getValue();
                            for (l=0; l<link.getContent().size(); l++)
                            {
                                if (link.getContent().get(l) instanceof R)
                                {
                                    TxtDoc+="\n{";
                                    run=(R)link.getContent().get(l);
                                    for(i=0;i<run.getContent().size(); i++)
                                    {
                                        if (run.getRPr() != null)
                                        {
                                            if (run.getRPr().getRStyle() != null && run.getRPr().getRStyle() != null)
                                            {
                                                Nombre = run.getRPr().getRStyle().getVal();
                                                TxtDoc+=estilosD(doc, Nombre, fuentes, colores);
                                            }
                                        }
                                        if (run.getContent().get(i) instanceof JAXBElement)
                                        {
                                            templink2 = (JAXBElement) run.getContent().get(i);
                                            if (templink2.getValue() instanceof Text)
                                            {
                                                btexto=(Text)templink2.getValue();
                                                id = obtenerLink(doc, link.getId());
                                                TxtDoc+="{\\field{\\*\\fldinst { HYPERLINK \"" + id +"\"}}{\\fldrslt {"+ btexto.getValue() + "}}}";
                                            }
                                        }
                                    }
                                }
                                TxtDoc+="}\n";
                            }
                        }
                    }
                    //revisa cada uno de los run
                    if( temp1.getContent().get(k) instanceof R)
                    {
                        run=(R)temp1.getContent().get(k);
                        for(i=0;i<run.getContent().size(); i++)
                        {
                            // abre una llave para cada run
                            propiedades=run.getRPr();
                            TxtDoc+="\n{";
                            //en caso de que el run tenga propiedades analiza cada una de ellas
                            if(propiedades != null)
                            {
                                //Estilos Run Kenming C
                                if (run.getRPr().getRStyle() != null)
                                {
                                    band = 1;
                                    if (run.getRPr().getRStyle() != null)
                                    {
                                        Nombre = run.getRPr().getRStyle().getVal();
                                        TxtDoc+=estilosD(doc, Nombre, fuentes, colores);
                                    }
                                }
                                if(propiedades.getB()!=null && propiedades.getB().isVal())
                                {
                                    TxtDoc+="\\b ";//negritas
                                }
                                if(propiedades.getI()!=null && propiedades.getI().isVal())
                                {
                                    TxtDoc+="\\i ";// italicas
                                }
                                if(propiedades.getU()!=null)
                                {    
                                    TxtDoc+="\\ul "; //texto subrayado get U ya evalua directamente 
                                }
                                if(propiedades.getStrike() !=null && propiedades.getStrike().isVal())
                                {  
                                    TxtDoc+="\\striked1 "; //texto tachado 
                                }
                                if(propiedades.getHighlight()!= null && propiedades.getHighlight().getVal() != null )
                                {
                                    int c=0;
                                    int bdh=0;
                                    for(c=0; c<resal.size(); c++)
                                    {
                                        //Resaltado
                                        if(((String)resal.get(c)).equals(propiedades.getHighlight().getVal()))
                                        {
                                            bdh=1;
                                            break;
                                        }
                                    }
                                    if(bdh==1)
                                    {
                                        TxtDoc+=" \\highlight"+c+" ";
                                    }
                                    else
                                    {
                                        TxtDoc+=" \\highlight"+6+" ";
                                    }
                                    
                                }
                                //obtiene el tipo de fuente
                                if(propiedades.getRFonts()!=null) 
                                {
                                    font= new Fuentes(propiedades.getRFonts().getAscii());
                                    if(font.getNombre()!=null)
                                    {
                                        int c=0;
                                        for(c=0; c<fuentes.size(); c++)
                                        {
                                            if(font.equals(fuentes.get(c)))
                                            {
                                                bdf=1;
                                                break;
                                            }
                                        }
                                        if(bdf==0)
                                        {
                                            fuentes.add((Fuentes)font);
                                            
                                            this.cabezaF+="{\\f"+c+" \\fmodern \\fcharset0 "+propiedades.getRFonts().getAscii()+" }";
                                        }
                                        TxtDoc+="\\f"+c+" ";
                                        bdf=0;
                                        
                                    }
                                }
                                //Tamaño de letra
                                if(propiedades.getSz()!=null)//obtiene el  tamaño
                                {
                                    TxtDoc+= "\\fs"+propiedades.getSz().getVal()+" ";
                                }
                                if(propiedades.getColor() != null && !propiedades.getColor().getVal().equals("auto"))//asigna el color
                                {
                                    color=CrearColor(propiedades);//esta funcion esta en la parte de abajo
                                    int c=0;
                                    for(c=0; c<colores.size(); c++)
                                    {
                                        if(color.equals(colores.get(c)))
                                        {
                                            bd=1;
                                            break;
                                        }
                                    }
                                    // en caso de que el color del run no este en la lista de
                                    //colores conocidos, se lo añade a la lista y a la cabecera
                                    if(bd==0)   
                                    {
                                        colores.add((Colores)color);
                                        
                                        cabezaC+="\\red"+color.getRed()+"\\green"+color.getGreen()+ "\\blue" +color.getBlue()+";";
                                    }
                                    TxtDoc+="\\cf"+c+" ";
                                    bd=0;
                                }
                                else 
                                {
                                    if (band == 0)
                                    TxtDoc+="\\cf0 ";
                                }
                            }
                            if(run.getContent().get(i) instanceof JAXBElement)
                            {
                               temp2=(JAXBElement)run.getContent().get(i);
                               if(temp2.getValue() instanceof Drawing)
                                {
                                    Drawing draw=(Drawing)temp2.getValue();
                                    //descomprime el .docx
                                    unZipIt(docZip, "..\\Unzip\\");
                                    if(draw.getAnchorOrInline().get(0) instanceof Inline)
                                    {
                                        Inline line=(Inline)draw.getAnchorOrInline().get(0);
                                        String name=line.getGraphic().getGraphicData().getPic().getNvPicPr().getCNvPr().getName();
                                        imgCont++;
                                        try {
                                            //obtiene la imagen en hex
                                            TxtDoc+=ObtenerImagen("..\\Unzip\\word\\media\\image"+imgCont+name.substring(name.lastIndexOf(".")));
                                        } catch (IOException ex) {
                                            System.out.println("Fallo");
                                        }                                    
                                    }
                                }
                                else if(temp2.getValue() instanceof Text)
                                {
                                    btexto=(Text)temp2.getValue();
                                    TxtDoc+=btexto.getValue();
                                }
                            }
                            //TxtDoc+="{\\ftnbj " +footer(doc)+" Pagina:\\chpgn}";
                            TxtDoc+="}\n";
                        }
                    }
                }
                TxtDoc+="\n"+ParProp;
            }            
        }
        TxtDoc+="{\\ftntj " +footer(doc)+" Pagina:\\chpgn}";
        TxtDoc+="{\\header " +header(doc)+"}\n";
        TxtDoc=CaracteresEspeciales(TxtDoc);
        this.cabezaF+="}";
        this.cabezaC+="}";
        return TxtDoc;
    }
    
    //Marginado
    public ArrayList margins(WordprocessingMLPackage doc)
    {
        ArrayList<Integer>margen=new ArrayList();
        margen.add(doc.getMainDocumentPart().getJaxbElement().getBody().getSectPr().getPgMar().getTop().intValue());
        margen.add(doc.getMainDocumentPart().getJaxbElement().getBody().getSectPr().getPgMar().getLeft().intValue());
        margen.add(doc.getMainDocumentPart().getJaxbElement().getBody().getSectPr().getPgMar().getRight().intValue());
        margen.add(doc.getMainDocumentPart().getJaxbElement().getBody().getSectPr().getPgMar().getBottom().intValue());
        return margen;
    }
    
    //Cabecera
    public String header(WordprocessingMLPackage doc)
    {
        String encaB="";
        int i;
        List<SectionWrapper> sW= doc.getDocumentModel().getSections();
        for(SectionWrapper iterador: sW)
        {
            HeaderPart  headerP= iterador.getHeaderFooterPolicy().getDefaultHeader();
            if (headerP != null && headerP.getContent() != null)
            {
                for(i=0; i<headerP.getContent().size(); i++)
                {
                    if(headerP.getContent().get(i) instanceof P)
                    {
                        P par=(P)headerP.getContent().get(i);
                        for(int j=0; j<par.getContent().size(); j++)
                        {
                            if(par.getContent().get(j) instanceof R)
                            {
                                R run=(R)par.getContent().get(j);
                                for(int Z=0; Z<run.getContent().size(); Z++)
                                {
                                    if(run.getContent().get(Z) instanceof JAXBElement)
                                    {
                                        JAXBElement temp=((JAXBElement)run.getContent().get(Z));

                                        if(temp.getValue() instanceof Text)
                                        {
                                            Text texto= (Text)temp.getValue();
                                            encaB+=texto.getValue();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }    
        }
        return encaB;
    }
    
    //Pie de pagina
    public String footer(WordprocessingMLPackage doc)
    {
        String pieP="";
        int i;
        List<SectionWrapper> sW= doc.getDocumentModel().getSections();
        for(SectionWrapper iterador: sW)
        {
            FooterPart  footerP= iterador.getHeaderFooterPolicy().getDefaultFooter();
            if(footerP != null && footerP.getContent() !=null)
            {
               for(i=0; i<footerP.getContent().size(); i++)
                {
                    if(footerP.getContent().get(i) instanceof P)
                    {
                        P par=(P)footerP.getContent().get(i);
                        for(int j=0; j<par.getContent().size(); j++)
                        {
                            if(par.getContent().get(j) instanceof R)
                            {
                                R run=(R)par.getContent().get(j);
                                for(int Z=0; Z<run.getContent().size(); Z++)
                                {
                                    if(run.getContent().get(Z) instanceof JAXBElement)
                                    {
                                        JAXBElement temp=((JAXBElement)run.getContent().get(Z));

                                        if(temp.getValue() instanceof Text)
                                        {
                                            Text texto= (Text)temp.getValue();
                                            pieP+=texto.getValue();
                                        }
                                    }
                                }
                            }
                        }
                    }
                } 
            }
        }
        return pieP;
    }
    
    //Estilos        
    public String estilosD(WordprocessingMLPackage doc, String nombre, ArrayList fuentes, ArrayList colores)
    {
        Styles s;
        s = doc.getMainDocumentPart().getStyleDefinitionsPart().getJaxbElement();
        RPr estilosRun = null;
        Fuentes font;
        Colores color;
        int i=0, bdf=0, bd=0;
        String Stilos="";
        for(i=0; i< s.getStyle().size(); i++)
        {
            if (s.getStyle().get(i).getStyleId().equals(nombre))
            {
                estilosRun = s.getStyle().get(i).getRPr();
                if (estilosRun!=null && estilosRun.getB() != null && estilosRun.getB().isVal())
                {
                    Stilos+="\\b ";
                }
                if(estilosRun!=null &&estilosRun.getI()!= null && estilosRun.getI().isVal())
                {
                    Stilos+="\\i ";
                }
                if(estilosRun!=null && estilosRun.getU()!= null)
                {    
                    Stilos+="\\ul "; 
                }
                if(estilosRun!=null && estilosRun.getStrike() != null && estilosRun.getStrike().isVal())
                {  
                    Stilos+="\\striked1 ";  
                }
                if (estilosRun!=null && estilosRun.getSz() != null)
                {
                    Stilos+="\\fs" + estilosRun.getSz().getVal() + " ";
                }
                if(estilosRun!=null && estilosRun.getRFonts()!= null)
                {
                    font= new Fuentes(estilosRun.getRFonts().getAscii());
                    if( font.getNombre()!=null)
                    {
                        int c=0;
                        for(c=0; c<fuentes.size(); c++)
                        {
                            if(font.getNombre()!=null && font.equals(fuentes.get(c)))
                            {
                                bdf=1;
                                break;
                            }
                        }
                        if(bdf==0)
                        {
                            fuentes.add((Fuentes)font);
                            this.cabezaF+="{\\f"+c+" \\fmodern \\fcharset0 "+estilosRun.getRFonts().getAscii()+" }";
                        }
                        bdf=0;
                        Stilos+="\\f"+c+" ";
                    }
                }
                if(estilosRun!=null && estilosRun.getColor() != null)
                {
                    color=CrearColor(estilosRun);
                    int c=0;
                    for(c=0; c<colores.size(); c++)
                    {
                        if(color.equals(colores.get(c)))
                        {
                            bd=1;
                            break;
                        }
                    }
                    if(bd==0)   
                    {
                        colores.add((Colores)color);
                        cabezaC+="\\red"+color.getRed()+"\\green"+color.getGreen()+ "\\blue" +color.getBlue()+";";
                    }
                    bd=0;
                    Stilos+="\\cf"+c+" ";
                }
            }
        }
        return Stilos;
    }
    
    //Obtener el link del hipervinculo
    public String obtenerLink (WordprocessingMLPackage doc, String id) throws Docx4JException
    {
        List relaciones = new ArrayList();
        Relationship rel=null;
        int i=0;
        String target="";
        relaciones = doc.getMainDocumentPart().getRelationshipsPart().getContents().getRelationship();
        for (i=0; i<relaciones.size(); i++)
        {
            rel = (Relationship) relaciones.get(i);
            if (rel.getId().equals(id))
            {
                target = rel.getTarget();
            }
        } 
        return target;
    }
    
    //Alineacion de tablas
    public String DetAlign(Tbl tabla)
    {
        String align="";
        switch(tabla.getTblPr().getJc().getVal())
        {
            case BOTH:
            {
                align="c";
                break;
            }
            case RIGHT:
            {
                align="r";
                break;
            }
            case LEFT:
            {
                align="l";
                break;
            }
            default:
            {
                align="c";
                break;
            }
        }
        return align;
    }
    
    //Genera el color
    public Colores CrearColor(RPr propiedades)
    {
        int multi=0, sum=0,red=0,green=0,blue=0, cont=0;
        Colores color;
        do{
            String col=propiedades.getColor().getVal().substring(cont*2, (cont*2)+2);                                                   
            switch(col.substring(0,1))
            {
                case "0":
                        {
                          multi=0;
                          break;
                        }
                case "1":
                        {
                          multi=1;
                          break;
                        }
                case "2":
                        {
                          multi=2;
                          break;
                        }
                case "3":
                        {
                          multi=3;  
                          break;
                        }
                case "4":
                        {
                          multi=4;  
                          break;
                        }
                case "5":
                        {
                          multi=5;
                          break;
                        }
                case "6":
                        {
                          multi=6;
                          break;
                        }
                case "7":
                        {
                          multi=7;  
                          break;
                        }
                case "8":
                        {
                          multi=8;
                          break;
                        }
                case "9":
                        {
                          multi=9;
                          break;
                        }
                case "A":
                        {
                          multi=10;
                          break;
                        }
                case "B":
                        {
                          multi=11;  
                          break;
                        }
                case "C":
                        {
                          multi=12;
                          break;
                        }
                case "D":
                        {
                          multi=13;
                          break;
                        }
                case "E":
                        {
                          multi=14;
                          break;
                        }
                case "F":
                        {
                          multi=15;
                          break;
                        }
            }

            switch(col.substring(1,2))
            {
                case "0":
                        {
                          sum=0;
                          break;
                        }
                case "1":
                        {
                          sum=1;
                          break;
                        }
                case "2":
                        {
                          sum=2;
                          break;
                        }
                case "3":
                        {
                          sum=3;
                          break;
                        }
                case "4":
                        {
                          sum=4;
                          break;
                        }
                case "5":
                        {
                          sum=5;
                          break;
                        }
                case "6":
                        {
                          sum=6;
                          break;
                        }
                case "7":
                        {
                          sum=7;
                          break;
                        }
                case "8":
                        {
                          sum=8;
                          break;
                        }
                case "9":
                        {
                          sum=9;
                          break;
                        }
                case "A":
                        {
                          sum=10;
                          break;
                        }
                case "B":
                        {
                          sum=11;
                          break;
                        }
                case "C":
                        {
                          sum=12;
                          break;
                        }
                case "D":
                        {
                          sum=13;
                          break;
                        }
                case "E":
                        {
                          sum=14;
                          break;
                        }
                case "F":
                        {
                          sum=15;
                          break;
                        }
            }
            switch (cont)
            {
                case 0:
                {
                    red=(multi*16)+sum;
                    break;
                }
                case 1:
                {
                    green=(multi*16)+sum;
                    break;
                }
                case 2:
                {
                    blue=(multi*16)+sum;
                    break;
                }
            }
            cont++;
        }while(cont<3);
        color=new Colores(red,green,blue);
        return color;
    }
    
    //Remplaza los caracteres especiales
    public String CaracteresEspeciales(String TxtDoc)
    {
        TxtDoc=TxtDoc.replaceAll("í", "\\\\'ed");
        TxtDoc=TxtDoc.replaceAll("ó", "\\\\'f3");
        TxtDoc=TxtDoc.replaceAll("ú", "\\\\'fa");
        TxtDoc=TxtDoc.replaceAll("Á", "\\\\'c1");
        TxtDoc=TxtDoc.replaceAll("É", "\\\\'c9");
        TxtDoc=TxtDoc.replaceAll("Í", "\\\\'cd");
        TxtDoc=TxtDoc.replaceAll("Ó", "\\\\'d3");
        TxtDoc=TxtDoc.replaceAll("Ú", "\\\\'da");       
        TxtDoc=TxtDoc.replaceAll("¿", "\\\\'bf");
        TxtDoc=TxtDoc.replaceAll("¡", "\\\\'a1");
        TxtDoc=TxtDoc.replaceAll("´", "\\\\'b4");
        TxtDoc=TxtDoc.replaceAll("ü", "\\\\'fc");
        TxtDoc=TxtDoc.replaceAll("Ü", "\\\\'dc");
        TxtDoc=TxtDoc.replaceAll("ý", "\\\\'fd");
        TxtDoc=TxtDoc.replaceAll("Ý", "\\\\'dd");
        TxtDoc=TxtDoc.replaceAll("ñ", "\\\\'f1");
        TxtDoc=TxtDoc.replaceAll("’", "\\\\rquote ");
        TxtDoc=TxtDoc.replaceAll("á", "\\\\'e1");
        TxtDoc=TxtDoc.replaceAll("é", "\\\\'e9");
        
        
        return TxtDoc;
    }
    
    
    
    public String AccesarParrafos(List parf ,ArrayList resal, ArrayList colores, ArrayList fuentes, ArchivoRTF rtf, WordprocessingMLPackage doc )
    {
        R run;
        String TxtDoc="", Nombre="", ParProp="";
        Text btexto;
        Colores color;
        Fuentes font;
        int bd=0, bdf=0, band=0;
        RPr propiedades=null;
        JAXBElement temp2=null;
        int i=0,j=0,k=0;
        for(j=0; j<parf.size(); j++)
        {
            if(parf.get(j) instanceof P)
            {
                P temp1= (P)parf.get(j);
                if (temp1.getRsidRDefault() != null)
                {
                    TxtDoc+= "\\par\\par ";
                }
                TxtDoc+="\n{";                                          
                //obtiene el contenido del parrafo
                for(k=0; k<temp1.getContent().size(); k++)
                {
                    band = 0;
                    ParProp="";
                    if(temp1.getPPr() != null)
                    {
                        //contendfo de viñeta
                        /*if (temp1.getPPr().getPStyle()!= null && temp1.getPPr().getPStyle().getVal()!=null)
                        {   
                            TxtDoc+= "{\n\\pard}";
                            TxtDoc+= "{\\pntext\\f"+numeraF+"\\'B7\\tab}";
                            TxtDoc+="{\\*\\pn\\pnlvlblt\\pnf"+numeraF+"\\pnindent0{\\pntxtb\\'B7}}";
                            // 'B7 unicode de bala redonda
                            TxtDoc+=" ";
                        }*/
                        if( temp1.getPPr().getJc()!= null)
                        {
                            switch(temp1.getPPr().getJc().getVal())
                            {
                                case BOTH:
                                {
                                    ParProp+=" \\qj ";
                                    break;
                                }
                                case LEFT:
                                {
                                    ParProp+=" \\ql ";
                                    break;
                                }
                                case CENTER:
                                {
                                    ParProp+=" \\qc ";
                                    break;
                                }
                                case RIGHT:
                                {
                                    ParProp+=" \\qr ";
                                    break;
                                }
                            }
                        }
                        else
                        {
                            ParProp+=" \\ql ";
                        }
                        //Estilos Kenimg C.
                        if ( temp1.getPPr().getPStyle() != null && temp1.getPPr().getPStyle().getVal() != null)
                        {
                            band = 1;
                            Nombre = temp1.getPPr().getPStyle().getVal();
                            TxtDoc+=estilosD(doc, Nombre, fuentes, colores);
                        }
                        if(temp1.getPPr().getSpacing()!= null)
                        {
                            if( temp1.getPPr().getSpacing().getLine()!= null)
                            {
                                ParProp+=" \\sl"+temp1.getPPr().getSpacing().getLine().intValue()+" ";
                            }
                            else
                            {
                                ParProp+=" \\sl0 ";
                            }
                            if( temp1.getPPr().getSpacing().getAfter()!= null)
                            {
                                ParProp+=" \\sa"+temp1.getPPr().getSpacing().getAfter().intValue()+" ";
                            }
                            else
                            {
                                ParProp+=" \\sa0 ";
                            }
                            if( temp1.getPPr().getSpacing().getBefore()!= null)
                            {
                                ParProp+=" \\sb"+temp1.getPPr().getSpacing().getBefore().intValue()+" ";
                            }
                            else
                            {
                                ParProp+="\\sb0 ";   
                            }    
                        }
                        else
                        {
                            ParProp+=" \\sl0 \\sa0 \\sb0";
                        }
                        if(temp1.getPPr().getShd() != null && temp1.getPPr().getShd().getFill() != null)
                        {
                            color=CrearColor(temp1.getPPr().getShd().getFill());//esta funcion esta en la parte de abajo
                            int c=0;
                            for(c=0; c<colores.size(); c++)
                            {
                                if(color.equals(colores.get(c)))
                                {
                                    bd=1;
                                    break;
                                }
                            }
                            // en caso de que el color del run no este en la lista de
                            //colores conocidos, se lo añade a la lista y a la cabecera
                            if(bd==0)   
                            {
                                colores.add((Colores)color);

                                cabezaC+="\\red"+color.getRed()+"\\green"+color.getGreen()+ "\\blue" +color.getBlue()+";";
                            }
                            ParProp+="\\cbpat"+c+" ";
                            bd=0;
                        }
                        else
                        {
                            ParProp+="\\cbpat";
                        }
                        if(temp1.getPPr().getInd() != null)
                        {
                            if(temp1.getPPr().getInd().getFirstLine()!= null)
                            {
                                ParProp+=" \\fi"+temp1.getPPr().getInd().getFirstLine().intValue()+" ";
                            }
                            else
                            {
                                ParProp+=" \\fi0 ";
                            }
                            if(temp1.getPPr().getInd().getLeft()!= null)
                            {
                                ParProp+=" \\li"+temp1.getPPr().getInd().getLeft().intValue()+" ";
                            }
                            else
                            {
                                ParProp+=" \\li0 ";
                            }
                            if(temp1.getPPr().getInd().getRight()!= null)
                            {
                                ParProp+=" \\ri"+temp1.getPPr().getInd().getRight().intValue()+" ";
                            }
                            else
                            {
                                ParProp+=" \\ri0 ";
                            }
                        }
                        else
                        {
                            ParProp+="\\li0 \\fi0 \\ri0 ";
                        }
                        
                    }
                    else
                    {
                        ParProp+="\\ql \\sl0 \\sa0 \\sb0 \\li0 \\fi0 \\ri0 \\cbpat ";
                    }
                    //revisa cada uno de los run
                    if( temp1.getContent().get(k) instanceof R)
                    {
                        run=(R)temp1.getContent().get(k);
                        for(i=0;i<run.getContent().size(); i++)
                        {
                            // abre una llave para cada run
                            propiedades=run.getRPr();
                            TxtDoc+="\n{";
                            //en caso de que el run tenga propiedades analiza cada una de ellas
                            if(propiedades != null)
                            {
                                //Estilos Run Kenming C
                                if (run.getRPr().getRStyle() != null && run.getRPr().getRStyle() != null)
                                {
                                    Nombre = run.getRPr().getRStyle().getVal();
                                    TxtDoc+=estilosD(doc, Nombre, fuentes, colores);
                                }
                                if(propiedades.getB()!=null && propiedades.getB().isVal())
                                {
                                    TxtDoc+=" \\b ";//negritas
                                }
                                if(propiedades.getI()!=null && propiedades.getI().isVal())
                                {
                                    TxtDoc+=" \\i ";// italicas
                                }
                                if(propiedades.getU()!=null)
                                {    
                                    TxtDoc+=" \\ul"; //texto subrayado get U ya evalua directamente 
                                }
                                if(propiedades.getStrike() !=null && propiedades.getStrike().isVal() )
                                {  
                                    TxtDoc+=" \\striked1"; //texto tachado 
                                }
                                if(propiedades.getHighlight()!= null && propiedades.getHighlight().getVal() != null )
                                {
                                    int c=0;
                                    int bdh=0;
                                    for(c=0; c<resal.size(); c++)
                                    {
                                        if(((String)resal.get(c)).equals(propiedades.getHighlight().getVal()))
                                        {
                                            bdh=1;
                                            break;
                                        }
                                    }
                                    if(bdh==1)
                                    {
                                        TxtDoc+=" \\highlight"+c+" ";
                                    }
                                    else
                                    {
                                        TxtDoc+=" \\highlight"+6+" ";
                                    }
                                    
                                }
                                //obtiene el tipo de fuente
                                if(propiedades.getRFonts()!=null) 
                                {
                                    font= new Fuentes(propiedades.getRFonts().getAscii());
                                    if(font.getNombre()!=null)
                                    {
                                        int c=0;
                                        for(c=0; c<fuentes.size(); c++)
                                        {
                                            if(font.equals(fuentes.get(c)))
                                            {
                                                bdf=1;
                                                break;
                                            }
                                        }
                                        if(bdf==0)
                                        {
                                            fuentes.add((Fuentes)font);
                                            this.cabezaF+="{\\f"+c+" \\fmodern \\fcharset0 "+propiedades.getRFonts().getAscii()+" }";
                                            
                                        }
                                        bdf=0;
                                        TxtDoc+="\\f"+c+" ";
                                    }
                                }
                                if(propiedades.getSz()!=null)//obtiene el  tamaño
                                {
                                    TxtDoc+= "\\fs"+propiedades.getSz().getVal()+" ";
                                }
                                if(propiedades.getColor() != null)//asigna el color
                                {
                                    color=CrearColor(propiedades);//esta funcion esta en la parte de abajo
                                    int c=0;
                                    for( c=0; c<colores.size(); c++)
                                    {
                                        if(color.equals(colores.get(c)))
                                        {
                                            bd=1;
                                            break;
                                        }
                                    }
                                    // en caso de que el color del run no este en la lista de
                                    //colores conocidos, se lo añade a la lista y a la cabecera
                                    if(bd==0)   
                                    {
                                        colores.add((Colores)color);
                                        cabezaC+="\\red"+color.getRed()+"\\green"+color.getGreen()+ "\\blue" +color.getBlue()+";";
                                        
                                    }
                                    bd=0;
                                    TxtDoc+="\\cf"+c+" ";
                                }
                                else 
                                {
                                    if (band == 0)
                                    TxtDoc+="\\cf0";
                                }
                            }
                            if(run.getContent().get(i) instanceof JAXBElement)
                            {
                               temp2=(JAXBElement)run.getContent().get(i);
                               if(temp2.getValue() instanceof Text)
                               {
                                   btexto=(Text)temp2.getValue();
                                   TxtDoc+=btexto.getValue();
                               }
                            }
                            
                            TxtDoc+="}\n";
                        }
                    }
                }
                TxtDoc+="}\n"+ParProp;
            }
        }
        return TxtDoc;
    }
    
    
    
    //Codigo obtenido de: http://www.mkyong.com/java/how-to-decompress-files-from-a-zip-file/
    //Descomprime zip
    public void unZipIt(String direccionzip, String direccioncarpeta)
    {
        byte[] buffer = new byte[1024];
 
        try{

           //create output directory is not exists
           File folder = new File(direccioncarpeta);
           if(!folder.exists()){
                   folder.mkdir();
           }

           //get the zip file content
           ZipInputStream zis = new ZipInputStream(new FileInputStream(direccionzip));
           //get the zipped file list entry
           ZipEntry ze = zis.getNextEntry();

           while(ze!=null){

              String fileName = ze.getName();
              File newFile = new File(direccioncarpeta + File.separator + fileName);

              System.out.println("file unzip : "+ newFile.getAbsoluteFile());

               //create all non exists folders
               //else you will hit FileNotFoundException for compressed folder
               new File(newFile.getParent()).mkdirs();

               try (FileOutputStream fos = new FileOutputStream(newFile)) {
                   int len;
                   while ((len = zis.read(buffer)) > 0) {
                       fos.write(buffer, 0, len);
                   }
               }
               ze = zis.getNextEntry();
           }

           zis.closeEntry();
           zis.close();

           System.out.println("Done");

       }catch(IOException ex){
       }
    }
    
    public String ObtenerImagen(String dir) throws IOException
    {
        BufferedImage image;
        ByteArrayOutputStream bArray = new ByteArrayOutputStream();
        File imagen= new File(dir);
        String tipo = dir.substring(dir.lastIndexOf("."));
        String total="\\pict";
        switch(tipo)
        {
            case".emf":
            {
                total+="\\emfblip";
                break;
            }
            case".png":
            {
                total+="\\pngblip";
                break;
            }
            case".jpeg":
            {
                total+="\\jpegblip";
                break;
            }
            case".bitmap":
            {
                total+="\\dibitmap0";
                break;
            }
        }
        total+="\\wbmbitspixel24\n";
        if(tipo.equals(".bitmap"))
        {
            total+="\\picw"+"\\pich";
        }
        String hex = javax.xml.bind.DatatypeConverter.printHexBinary(Files.toByteArray(imagen));
        total+=hex;
        return total;
        
        
    }
    public void llenarResal(ArrayList<String> resal, ArrayList<Colores> colores)
    {
        resal.add("black");
        resal.add("blue");
        resal.add("cyan");
        resal.add("green");
        resal.add("magenta");
        resal.add("red");
        resal.add("yellow");
        resal.add("darkBlue");
        resal.add("darkCyan");
        resal.add("darkGreen");
        resal.add("darkMagenta");
        resal.add("darkRed");
        resal.add("darkYellow");
        resal.add("darkGray");
        resal.add("lightGray");
        colores.add(new Colores(0,0,0));
        colores.add(new Colores(0,0,255));
        colores.add(new Colores(0,255,255));
        colores.add(new Colores(0,255,0));
        colores.add(new Colores(255,0,255));
        colores.add(new Colores(255,0,0));
        colores.add(new Colores(255,255,0));
        colores.add(new Colores(0,0,102));
        colores.add(new Colores(0,102,102));
        colores.add(new Colores(0,102,0));
        colores.add(new Colores(102,0,102));
        colores.add(new Colores(102,0,0));
        colores.add(new Colores(102,102,0));
        colores.add(new Colores(64,64,64));
        colores.add(new Colores(192,192,192));
    }
    public Colores CrearColor(String cHex)
    {
        int multi=0, sum=0,red=0,green=0,blue=0, cont=0;
        Colores color;
        do{
            String col=cHex.substring(cont*2, (cont*2)+2);                                                   
            switch(col.substring(0,1))
            {
                case "0":
                        {
                          multi=0;
                          break;
                        }
                case "1":
                        {
                          multi=1;
                          break;
                        }
                case "2":
                        {
                          multi=2;
                          break;
                        }
                case "3":
                        {
                          multi=3;  
                          break;
                        }
                case "4":
                        {
                          multi=4;  
                          break;
                        }
                case "5":
                        {
                          multi=5;
                          break;
                        }
                case "6":
                        {
                          multi=6;
                          break;
                        }
                case "7":
                        {
                          multi=7;  
                          break;
                        }
                case "8":
                        {
                          multi=8;
                          break;
                        }
                case "9":
                        {
                          multi=9;
                          break;
                        }
                case "A":
                        {
                          multi=10;
                          break;
                        }
                case "B":
                        {
                          multi=11;  
                          break;
                        }
                case "C":
                        {
                          multi=12;
                          break;
                        }
                case "D":
                        {
                          multi=13;
                          break;
                        }
                case "E":
                        {
                          multi=14;
                          break;
                        }
                case "F":
                        {
                          multi=15;
                          break;
                        }
            }

            switch(col.substring(1,2))
            {
                case "0":
                        {
                          sum=0;
                          break;
                        }
                case "1":
                        {
                          sum=1;
                          break;
                        }
                case "2":
                        {
                          sum=2;
                          break;
                        }
                case "3":
                        {
                          sum=3;
                          break;
                        }
                case "4":
                        {
                          sum=4;
                          break;
                        }
                case "5":
                        {
                          sum=5;
                          break;
                        }
                case "6":
                        {
                          sum=6;
                          break;
                        }
                case "7":
                        {
                          sum=7;
                          break;
                        }
                case "8":
                        {
                          sum=8;
                          break;
                        }
                case "9":
                        {
                          sum=9;
                          break;
                        }
                case "A":
                        {
                          sum=10;
                          break;
                        }
                case "B":
                        {
                          sum=11;
                          break;
                        }
                case "C":
                        {
                          sum=12;
                          break;
                        }
                case "D":
                        {
                          sum=13;
                          break;
                        }
                case "E":
                        {
                          sum=14;
                          break;
                        }
                case "F":
                        {
                          sum=15;
                          break;
                        }
            }
            switch (cont)
            {
                case 0:
                {
                    red=(multi*16)+sum;
                    break;
                }
                case 1:
                {
                    green=(multi*16)+sum;
                    break;
                }
                case 2:
                {
                    blue=(multi*16)+sum;
                    break;
                }
            }
            cont++;
        }while(cont<3);
        color=new Colores(red,green,blue);
        return color;
    }

}