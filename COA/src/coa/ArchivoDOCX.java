/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coa;
import java.io.File;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBElement;
import org.docx4j.*;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.FontTablePart;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.wml.P;
import org.docx4j.wml.R;
import org.docx4j.wml.RPr;
import org.docx4j.wml.Text;


/**
 *
 * @author Owner
 */
public class ArchivoDOCX 
{
    public WordprocessingMLPackage OpenDocx(String s)
    {
        //C:\Users\Owner\Documents\Christian\ha.txt
        Scanner in=new Scanner(System.in);
        String tipo, defecto=".docx";
        int i=s.indexOf(".");
        WordprocessingMLPackage Docx=null;
        tipo=s.substring(i);
        while(!tipo.equals(defecto))
        {
            System.out.println("El archivo entregado no es del tipo .DOCX\n");
            System.out.println("Ingrese una direccion Docx:");
            s = in.nextLine();
            i=s.indexOf(".");
            tipo=s.substring(i);
        }
        try {

            Docx = Docx4J.load(new File(s));
        } catch (Docx4JException ex) {
            Logger.getLogger(ArchivoDOCX.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Docx;
    }
    
}
