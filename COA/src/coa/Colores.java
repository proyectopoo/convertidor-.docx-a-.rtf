
package coa;

/**
 *
 * @author Gnimnek
 */
public class Colores 
{
    int red;
    int green;
    int blue;

    public Colores(int red, int green, int blue) 
    {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public int getRed() {
        return red;
    }

    public void setRed(int red) {
        this.red = red;
    }

    public int getGreen() {
        return green;
    }

    public void setGreen(int green) {
        this.green = green;
    }

    public int getBlue() {
        return blue;
    }

    public void setBlue(int blue) {
        this.blue = blue;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + this.red;
        hash = 37 * hash + this.green;
        hash = 37 * hash + this.blue;
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
            return false;
        else if (obj == this)
            return true;
        else if (obj instanceof Colores)
        {
            Colores col3 = (Colores) obj;
            if (this.getRed() == col3.getRed() && this.getBlue() == col3.getBlue() && this.getGreen() == col3.getGreen())
                return true;
        }
        return false;
    }
    
    
}
