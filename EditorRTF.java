/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coa;

import com.google.common.io.Files;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import static javafx.scene.paint.Color.color;
import javafx.scene.text.Text;

/**
 *
 * @author Administrador
 */
public class EditorRTF {
    private File archivo;
    private String txto;
    private String cabezaP= "{\\rtf1";
    private String cabezaC= "";
    private String cabezaF= "";
    
        public EditorRTF() 
    {
        cabezaC+="{\\colortbl;";
        cabezaF+="{\\fonttbl";
    }
    
   
    public File CrearRTF(String nomb, String dir) throws IOException
    {
        File nueRTF= new File(dir+nomb+".rtf");
        BufferedWriter bw;
        if(nueRTF.createNewFile())
        {
            bw = new BufferedWriter(new FileWriter(nueRTF));
            bw.write(cabezaP);
            bw.write(txto);
            bw.write("}");
            bw.close();
            
        }
        return nueRTF;
        
    }
      
    
    public String AgregarTexto (){
        
        String nueText ;
       
        Scanner in=new Scanner(System.in);
        System.out.println("ingrese texto ");
        nueText = in.nextLine();
        
        return nueText;
        
    }
        
    public String negrita(){
           String ne;
            ne="\\b";
            return ne; 
           
    }
     
     public String italica(){
           String ne;
            ne="\\i";
            return ne; 
           
    }
   
    public String tachada(){
           String ne;
            ne="\\strike";
            return ne; 
           
    }     
    
    public String subrayada(){
           String ne;
            ne="\\ul";
            return ne; 
           
    }     

    public String centrado(){
           String ne;
            ne="\\qc";
            return ne; 
    }  
    
    public String justificado(){
           String ne;
            ne="\\qj";
            return ne; 
    }  
        public String izquierda(){
           String ne;
            ne="\\ql";
            return ne; 
    }  
        public String derecha(){
           String ne;
            ne="\\qr";
            return ne; 
    }  
        
    public String AddImagen(String dir) throws IOException
    {
        File imagen= new File(dir);
        String tipo = dir.substring(dir.lastIndexOf("."));
        String total="\\pict";
        switch(tipo)
        {
            case".emf":
            {
                total+="\\emfblip";
                break;
            }
            case".png":
            {
                total+="\\pngblip";
                break;
            }
            case".jpeg":
            {
                total+="\\jpegblip";
                break;
            }
            case".bitmap":
            {
                total+="\\dibitmap0";
                break;
            }
        }
        total+="\\wbmbitspixel24\n";
        if(tipo.equals(".bitmap"))
        {
            total+="\\picw"+"\\pich";
        }
        String hex = javax.xml.bind.DatatypeConverter.printHexBinary(Files.toByteArray(imagen));
        total+=hex;
        return total;
        
        
    }
}
