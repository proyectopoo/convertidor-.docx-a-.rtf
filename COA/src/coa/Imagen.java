/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coa;

import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.ImageIcon;

/**
 *
 * @author Owner
 */
public class Imagen  extends javax.swing.JPanel
{

        public Imagen() 
        {
            this.setSize(3*550,3*900);
        }
        public void paint(Graphics graph)
        {
            Dimension dimensiones=getSize();
            ImageIcon ima= new ImageIcon(getClass().getResource("/imagenes/Happy_as_a_waiter.jpg"));
            graph.drawImage(ima.getImage(),40,0, dimensiones.height-775*3, dimensiones.width-475*3, null);
            setOpaque(false);
            super.paintComponent(graph);
        }
    
}
