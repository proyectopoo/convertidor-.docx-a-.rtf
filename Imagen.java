/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.JAXBElement;
import org.apache.xmlgraphics.image.loader.ImageInfo;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.WordprocessingML.BinaryPartAbstractImage;
import org.docx4j.openpackaging.parts.WordprocessingML.ImageJpegPart;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.wml.R;
import org.docx4j.wml.RPr;
import org.docx4j.wml.Text;

/**
 *
 * @author hernan
 */

public class Imagen {
    String tipo;
    

    
    public Imagen(String tipo){
        this.tipo = tipo;
    }
    
    public String gettipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
public String convertidorImagenes  (WordprocessingMLPackage doc)
    {
        
        
        
        String TxtDoc="", TxtCel="";
        Text btexto;
        
        MainDocumentPart Main = doc.getMainDocumentPart();
        
        
        ImageJpegPart ImageJpeg = doc.getParts();
        
        long ima=ImageJpeg.getContentLengthAsLoaded();//devuelve el tamaño en bytes de la parte almacenada 
        ImageInfo ima1 = ImageJpeg.getImageInfo();
        PartName ima3 = ImageJpeg.getPartName(); //devuelve ur
        
        HashMap<PartName, Part> abst = doc.getParts().getParts(); //retorna una hashmap de todas las partes 
        
        RelationshipsPart im1= doc.getRelationshipsPart();
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Imagen other = (Imagen) obj;
        return Objects.equals(this.tipo, other.tipo);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.tipo);
        return hash;
    }

    
}