/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectopoo;
import java.io.File;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBElement;
import org.docx4j.*;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.wml.P;
import org.docx4j.wml.R;
import org.docx4j.wml.RPr;
import org.docx4j.wml.Text;


/**
 *
 * @author hernan
 */
public class ArchivoDOCX {
    public WordprocessingMLPackage OpenDocx(String s)
    {
        //C:
        String tipo, defecto=".docx";
        int i=s.indexOf(".");
        WordprocessingMLPackage Docx=null;
        tipo=s.substring(i);
        if(!tipo.equals(defecto))
        {
            System.out.println("El archivo entregado no es del tipo .DOCX");
        }
        else
        {
            try {
                
                Docx = Docx4J.load(new File(s));
            } catch (Docx4JException ex) {
                Logger.getLogger(ArchivoDOCX.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return Docx;
    }
    public String ConvertirdorRTF (WordprocessingMLPackage doc)
    {
        List parf=new ArrayList();
        R run;
        String TxtDoc="";
        Text btexto;
        RPr propiedades=null;
        JAXBElement temp2=null;
        int i=0,j=0,k=0;
        MainDocumentPart Main = doc.getMainDocumentPart();
        for(j=0; j< Main.getContent().size(); j++)
        {
            parf.add(Main.getContent().get(j));
        }
        for(j=0; j<parf.size(); j++)
        {
            if(parf.get(j) instanceof P)
            {
                P temp1= (P)parf.get(j);
                for(k=0; k<temp1.getContent().size(); k++)
                {
                    if( temp1.getContent().get(k) instanceof R)
                    {
                        run=(R)temp1.getContent().get(k);
                        for(i=0;i<run.getContent().size(); i++)
                        {
                            propiedades=run.getRPr();
                            TxtDoc+="{";
                            if(propiedades!=null)
                            {
                                if(propiedades.getB()!=null && propiedades.getB().isVal())
                                {
                                    TxtDoc+="\\b ";
                                }
                                if(propiedades.getI()!=null && propiedades.getI().isVal())
                                {
                                    TxtDoc+="\\i ";
                                }
                                if(propiedades.getU()!=null)
                                {    
                                    TxtDoc+="\\ul"; //texto subrayado get U ya evalua directamente 
                                }
                                if(propiedades.getStrike() !=null && propiedades.getStrike().isVal())
                                {  
                                    TxtDoc+="\\striked1"; //texto tachado 
                                }
                            if(run.getContent().get(i) instanceof JAXBElement)
                            {
                               temp2=(JAXBElement)run.getContent().get(i);   
                            }
                            if(temp2.getValue() instanceof Text)
                            {
                                btexto=(Text)temp2.getValue();
                                TxtDoc+=btexto.getValue()+"}";
                                
                                
                            }
                        }
                    }
                }
            }            
        }

    }
  return TxtDoc;
  }
} 
    